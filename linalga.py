import numpy as np

def dot_prod(u,v):
    '''
    vector product
    
    input: two vectors of equal lenght
    output: scalar 
        
    '''
    if len(u) is not len(v): 
        print("Error: vectors do not have same lenght.")
    uv_prod = 0
    for i in range(len(u)):
        uv_prod += u[i]*v[i]
    return uv_prod


def gausselim(A, u):
    """this function carries out the Gaussian elimination routine to solve for v.
    First it row reduces, then back substitutes, and finishes by printing v."""
    
    u = u.astype(float)
    
    A = A.astype(float)
    
    if len(u) != len(A):
        print("Array and Matrix are not of the same length - computation cannot be completed")
         
    AA = np.vstack((A.T,u)).T
    i=0
    for i in range(len(AA)): #this is the row reduction
        if AA[i,i] !=0:
            AA[i] = AA[i]/AA[i,i] #make sure the pivot isn't zero (uses the first non-zero entry in each row)
        for j in range(i+1, len(AA)):
            AA[j] = AA[j] - AA[j,i]*AA[i]
    print(AA)   
    
    BB = np.copy(AA)
    m  = len(A)-1 
    v  = np.zeros(m+1,float) 
    u  = BB.T[-1] 
    AA = np.delete(BB,m+1,1) #this set of code sets up the back-substitution and deletes the u vector on the end of the matrix

    for i in range(m, -1, -1): #this is the back substitution; it starts with assigning v4 = 3 and using that to solve the rows above.
        v[i] = u[i]
        g = i
        while g < m:
            v[i] = v[i] - v[g+1]*AA[i, g+1]
            g = g +1

    print("v = ", v)