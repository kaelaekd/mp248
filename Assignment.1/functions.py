import numpy as np
e = np.e
def gauss(x, a = 1, b = 0, c = 1):
    """This function computes a Gaussian function. 
    It takes four parameters; a, b, and c are optional, x is the variable."""
    
    f = a*(e**(-((x-b)**2)/(2*(c**2))))
    return f

