def contplot():
    from numpy import exp,arange
    from pylab import meshgrid, cm, imshow, contour, clabel, colorbar, axis, title, show
    def h(x,y):
        return (exp(-(x**2 + y**2)))
 
    x = arange(-1.0, 1.2, 0.1)
    y = arange(-1.0, 1.5, 0.1)
    X,Y = meshgrid(x, y) 
    Z = h(X, Y) 

    im = imshow(Z, cmap=cm.RdBu) 

    cset = contour(Z, arange(-1,1.5,0.2), linewidths=2, cmap=cm.Set2)
    clabel(cset, inline=True, fmt='%1.1f', fontsize=10)
    colorbar(im) 

    title('$h = (-(X^2+y^2))$')
    show()
contplot()