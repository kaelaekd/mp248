import matplotlib
import numpy as np
import functions as fun

def sym_gauss_int_sqr(xb, n = 10):
    x = np.linspace((-xb), xb, n+1)
    f = fun.gauss(x, 1, 0, np.sqrt(.5))
    dx = (2*xb)/n
    answer = 0
    for i in range(n):
        answer = answer + f[i]*dx
    return answer**2